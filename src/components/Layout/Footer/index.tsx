import React from 'react';
import styles from './Footer.module.scss';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import moment from 'moment';

const Footer: React.FunctionComponent = () => {
  return (
    <Stack className={styles.footer}>
      <Stack horizontal horizontalAlign="space-between">
        <div>Copyright © {moment().year()} Company, All rights reserved.</div>
        <div>Powered by Company.</div>
      </Stack>
    </Stack>
  );
};

export default Footer;
