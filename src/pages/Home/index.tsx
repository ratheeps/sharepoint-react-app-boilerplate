import React, { FunctionComponent } from 'react';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Link } from 'react-router-dom';

const Home: FunctionComponent = () => {
  return (
    <Stack horizontalAlign="center" verticalAlign="center" styles={{ root: { height: '100vh' } }}>
      <Stack>
        <h1>Home</h1>
        <Link to="/customer">View Customer</Link>
      </Stack>
    </Stack>
  );
};

export default Home;
