import React, { FunctionComponent } from 'react';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { useLoadCustomers } from './hooks';

const Customer: FunctionComponent = () => {
  const { customers, isLoading } = useLoadCustomers();
  console.log(customers, isLoading);
  return (
    <Stack className="inner-page-panel">
      <h1>Customers</h1>
    </Stack>
  );
};

export default Customer;
