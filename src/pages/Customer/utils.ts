export const columns = [
  {
    key: 'id',
    name: 'ID',
    fieldName: 'id',
    isRowHeader: true,
    isResizable: true,
    minWidth: 20,
    maxWidth: 20,
    isSorted: true,
    sortAscendingAriaLabel: 'Sorted A to Z',
    sortDescendingAriaLabel: 'Sorted Z to A',
    data: 'number',
    isPadded: true
  },
  {
    key: 'code',
    name: 'Code',
    fieldName: 'code',
    isRowHeader: true,
    isResizable: true,
    minWidth: 150,
    maxWidth: 150,
    isSorted: true,
    sortAscendingAriaLabel: 'Sorted A to Z',
    sortDescendingAriaLabel: 'Sorted Z to A',
    data: 'string',
    isPadded: true,
    isSearchable: true
  },
  {
    key: 'display_name',
    name: 'Display Name',
    fieldName: 'display_name',
    isRowHeader: true,
    isResizable: true,
    minWidth: 150,
    maxWidth: 150,
    sortAscendingAriaLabel: 'sorted A to Z',
    sortDescendingAriaLabel: 'sorted Z to A',
    data: 'string',
    isPadded: true,
    isSearchable: true
  },
  {
    key: 'mobile',
    name: 'Mobile',
    fieldName: 'mobile',
    isRowHeader: true,
    isResizable: true,
    isSorted: true,
    sortAscendingAriaLabel: 'Sorted A to Z',
    sortDescendingAriaLabel: 'Sorted Z to A',
    data: 'string',
    isPadded: true,
    isSearchable: true
  }
];
