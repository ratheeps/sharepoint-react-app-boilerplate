import { useState, useEffect, useCallback } from 'react';
import { useAppValue } from '../../AppContext';

//Load customers
export function useLoadCustomers() {
  const [customers, setCustomers] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const context = useAppValue();

  const onLoadCustomers = useCallback(async () => {
    setLoading(true);
    try {
      const response: any = await context.customerService.get();
      setCustomers(response);
    } catch (e) {}
    setLoading(false);
  }, [context]);

  useEffect(() => {
    onLoadCustomers().then();
  }, [onLoadCustomers]);

  return { customers, isLoading };
}
