import React from 'react';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Spinner } from 'office-ui-fabric-react/lib/Spinner';

function AppLoading() {
  return (
    <Stack horizontalAlign="center" verticalAlign="center" styles={{ root: { height: '100vh' } }}>
      <Stack
        styles={{
          root: {
            backgroundColor: '#eaeaea',
            padding: '10px',
            paddingLeft: '20px',
            paddingRight: '20px'
          }
        }}
      >
        <Spinner />
      </Stack>
    </Stack>
  );
}

export default AppLoading;
