import React from 'react';
import { AppContext, useAppValue } from './AppContext';
import Routes from 'routes';

const App: React.FunctionComponent = () => {
  const appStore = useAppValue();

  return (
    <AppContext.Provider value={appStore}>
      <Routes />
    </AppContext.Provider>
  );
};

export default App;
