export const HOME = '/';

export const LIST_CUSTOMER = '/customer';
export const CREATE_CUSTOMER = '/customer/create';
export const VIEW_CUSTOMER = '/customer/:id';
export const EDIT_CUSTOMER = '/customer/:id/edit';
