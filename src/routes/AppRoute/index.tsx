import React, { Suspense, FunctionComponent } from 'react';
import { Route } from 'react-router-dom';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import Footer from 'components/Layout/Footer';
import AppRouterInterface from './types/index';

const AppRoute: FunctionComponent<AppRouterInterface> = ({ fallback, ...props }) => {
  const Component: any = props.component;
  return (
    <Suspense fallback={fallback}>
      <Route
        {...props}
        render={renderProps => {
          return (
            <Stack styles={{ root: { flex: 1 } }}>
              <Stack styles={{ root: { flex: 1 } }}>
                <Component {...renderProps} />
              </Stack>
              <Stack styles={{ root: { flex: 1 } }}>
                <Footer />
              </Stack>
            </Stack>
          );
        }}
      />
    </Suspense>
  );
};

export default AppRoute;
