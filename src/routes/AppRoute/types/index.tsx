import { ComponentType, FunctionComponent, ReactNode } from 'react';
import * as H from 'history';
import { RouteChildrenProps, RouteComponentProps } from 'react-router';

export default interface AppRouterInterface {
  fallback: FunctionComponent<any>;
  location?: H.Location;
  component?: ComponentType<RouteComponentProps<any>> | ComponentType<any>;
  render?: (props: RouteComponentProps<any>) => ReactNode;
  children?: ((props: RouteChildrenProps<any>) => ReactNode) | ReactNode;
  path?: string | string[];
  exact?: boolean;
  sensitive?: boolean;
  strict?: boolean;
}
