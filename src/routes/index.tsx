import React, { lazy } from 'react';
import { Switch, BrowserRouter as Router } from 'react-router-dom';
import * as routes from '../constants/routes';
import Loader from './Loader';
import AppRoute from './AppRoute';

const Route: React.FunctionComponent = () => {
  return (
    <Router>
      <Switch>
        <AppRoute exact fallback={Loader} path={routes.HOME} component={lazy(() => import('../pages/Home/index'))} />
        <AppRoute exact fallback={Loader} path={routes.LIST_CUSTOMER} component={lazy(() => import('../pages/Customer/index'))} />
      </Switch>
    </Router>
  );
};

export default Route;
