import React, { FunctionComponent } from 'react';
import { Stack } from 'office-ui-fabric-react/lib/Stack';
import { Spinner, SpinnerSize } from 'office-ui-fabric-react/lib/Spinner';

const Loader: FunctionComponent = () => (
  <Stack verticalAlign="center" horizontalAlign="center" styles={{ root: { flex: 1, marginTop: 50 } }}>
    <Spinner label="Loading..." size={SpinnerSize.large} />
  </Stack>
);

export default Loader;
