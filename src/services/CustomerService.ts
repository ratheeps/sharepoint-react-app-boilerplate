import { sp } from '@pnp/sp';
import { CUSTOMER_LIST } from '../constants/list';

export class CustomerService implements CustomerServiceInterface {
  public async get() {
    return await sp.web.lists
      .getByTitle(CUSTOMER_LIST)
      .items.select('ID', 'Title')
      .get();
  }
}

export interface CustomerServiceInterface {
  get(): Promise<any>;
}
